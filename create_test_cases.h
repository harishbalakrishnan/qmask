#ifndef CREATE_TEST_CASES_H
#define CREATE_TEST_CASES_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QInputDialog>

namespace Ui {
class Create_Test_Cases;
}

class Create_Test_Cases : public QDialog
{
    Q_OBJECT
    
public:
    explicit Create_Test_Cases(QWidget *parent = 0);
    ~Create_Test_Cases();
    
private slots:
    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_10_clicked();

    void on_pushButton_11_clicked();

    void on_pushButton_12_clicked();

    void on_pushButton_13_clicked();

    void on_pushButton_14_clicked();

    void on_comboBox_currentIndexChanged(int index);

private:
    Ui::Create_Test_Cases *ui;
    QStringListModel *available_functions,*selected_functions,*model;
    QStringListModel *available_testCases,*selected_testCases,*model1;
    QStringList functions,sel_functions,testCases,sel_testCases;
};

#endif // CREATE_TEST_CASES_H
