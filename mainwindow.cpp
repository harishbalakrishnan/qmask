#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "hkp_profile.h"
#include "phone_profile.h"
#include "test_case_creation.h"
#include "calibration.h"
#include "create_test_cases.h"
#include "execution.h"
#include "phone_profile_deletion.h"
#include <QtCore>
#include <QtGui>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(ui->label);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionHKP_profile_triggered()
{
    // HKP profile

    HKP_Profile hkp;
    hkp.setModal(true);
    hkp.exec();
}

void MainWindow::on_actionPhone_profile_triggered()
{
    // Phone Profile
    Phone_Profile ph;
    ph.setModal(true);
    ph.exec();

}

void MainWindow::on_actionTest_case_creation_triggered()
{
    Test_Case_Creation test;
    test.setModal(true);
    test.exec();

}

void MainWindow::on_actionCalibration_triggered()
{
    // Calibration
    Calibration cal;
    cal.setModal(true);
    cal.exec();
}

void MainWindow::on_actionCreate_Test_Cases_triggered()
{
    // Create Test Cases.
    Create_Test_Cases new_dialog_box;
    new_dialog_box.setModal(true);
    new_dialog_box.exec();

}

void MainWindow::on_actionTest_Case_Execution_triggered()
{
    //Test Case Execution.
    Execution new_box;
    new_box.setModal(true);
    new_box.exec();
}

void MainWindow::on_actionPhone_Profile_Deletion_triggered()
{
    //Phone Profile Deletion
    Phone_Profile_Deletion new_del_box;
    new_del_box.setModal(true);
    new_del_box.exec();
}
