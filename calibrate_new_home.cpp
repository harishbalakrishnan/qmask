#include "calibrate_new_home.h"
#include "ui_calibrate_new_home.h"

//Main Directory
QString main_directory1="G:\\Qt_Projects\\Main_Proj\\Prototype_new";

//Current Phone profile folder name
QString phone_profile_name1="Sony";


//Current phone profile directory path
QString phone_profile_dir_path1="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Sony";

//Slot path
QString Slot_path1="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Nokia1100\\Slot1";

float find_delta1(QString path,QChar c)
{
    //This function finds delta.

    //Variables
    int i=0;
    float str_to_float=1;
    QChar uc,lc;
    QString temp_string="";

    //Code
    lc=c.toLower();
    uc=c.toUpper();
    QFile file1(path);
    file1.open(QFile::ReadWrite | QFile::Text);

    QTextStream inout(&file1);
    QString text=inout.readAll();
    while ( text[i]!=*text.end() )
    {
        if(text[i]==lc||text[i]==uc)
            {
                i++;
                while(text[i]!=' ')
                {
                    temp_string=temp_string.append(text[i]);
                    i++;
                }
                str_to_float=temp_string.toFloat();
                str_to_float=(str_to_float*1000);
                str_to_float=(int)str_to_float;
                str_to_float=str_to_float/1000;
                temp_string="";
                break;
            }
            i++;
   }
    file1.close();
    return (str_to_float);

}

void rewrite_func_with_delta1(QString path,QString path1)
{
    //Variables required
    QString temp_string="";
    int i=0,pos=0,len=0,indicator=0;
    float str_to_float=0,x_diff=0,y_diff=0,z_diff=0;

    //Code
    x_diff=find_delta1(path1,'X');
    y_diff=find_delta1(path1,'Y');
    z_diff=find_delta1(path1,'Z');

    QFile file1(path);
     if(!file1.open(QFile::ReadWrite | QFile::Text))
         qDebug () << "Could not open file";

     QTextStream inout(&file1);
     QString text=inout.readAll();

     while ( text[i]!=*text.end() )
     {
         if(text[i]=='X'||text[i]=='Y'||text[i]=='Z'||text[i]=='x'||text[i]=='y'||text[i]=='z')
             {
                 indicator=i;
                 i++;
                 pos =i;
                 while(text[i]!=' ')
                 {
                     temp_string=temp_string.append(text[i]);
                     i++;
                 }
                 len=temp_string.length();
                 str_to_float=temp_string.toFloat();
                 if(text[indicator]=='X'||text[indicator]=='x')
                    str_to_float+=x_diff;
                 else if(text[indicator]=='Y'||text[indicator]=='y')
                     str_to_float+=y_diff;
                 else
                     str_to_float+=z_diff;
                 str_to_float=(str_to_float*1000);
                 str_to_float=(int)str_to_float;
                 str_to_float=str_to_float/1000;
                 QString tt;
                 if((str_to_float<0.001)&&(str_to_float>-0.001))
                     tt="0";
                 else
                     QTextStream(&tt) <<str_to_float;

                 temp_string="";
                 text=text.replace(pos,len,tt);
                 i=pos+tt.length();

             }

             i++;
    }
    file1.remove();
    file1.close();
    file1.open(QFile::WriteOnly | QFile::Text |QFile::Append);
    QTextStream out(&file1);
    out<<text;
    out.flush();
    file1.close();

}

Calibrate_new_home::Calibrate_new_home(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Calibrate_new_home)
{
    ui->setupUi(this);
    ui->comboBox->addItem("Slot1");
    ui->comboBox->addItem("Slot2");
    ui->comboBox->addItem("Slot3");
    ui->comboBox->addItem("Slot4");

    QDir present_dir(main_directory1);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    model=new QStringListModel;
    model->setStringList(directories);
    ui->comboBox_2->setModel(model);



}

Calibrate_new_home::~Calibrate_new_home()
{
    delete ui;
}

void Calibrate_new_home::on_buttonBox_accepted()
{
        //Ok button
    phone_profile_name1= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path1=main_directory1+"\\"+phone_profile_name1;
    Slot_path1=phone_profile_dir_path1+"\\"+ui->comboBox->itemText(ui->comboBox->currentIndex());


    QString current_home_path=Slot_path1+"\\home.tap";
    QString new_home_path=Slot_path1+"\\home2.tap";
    QFile f1(new_home_path),f2(current_home_path);
             //home2             //home
    float x_d=0,x=0,y_d=0,y=0,z_d=0,z=0;

    x=find_delta1(new_home_path,'X');
    y=find_delta1(new_home_path,'Y');
    z=find_delta1(new_home_path,'Z');
    x_d=find_delta1(current_home_path,'X');
    y_d=find_delta1(current_home_path,'Y');
    z_d=find_delta1(current_home_path,'Z');
    qDebug () <<x<<" "<<y<<" "<<z<<" "<<x_d<<" "<<y_d<<" "<<z_d<<" ";
    x=x_d-x;
    y=y_d-y;
    z=z_d-z;
    f1.remove();
    f1.open(QFile::WriteOnly | QFile::Text);
    QTextStream out(&f1);
    out<<"G0 X"<<x<<" Y"<<y<<" Z"<<z<<" \n";
    f1.close();


    QDir pres_dir(Slot_path1);
    QStringList funct=pres_dir.entryList(QDir::Files);

    foreach(QString f,funct)                        //Slot_path has the current slot's path
    {
        if((f!="home.tap")&&(f!="home2.tap"))
        {
            QString filepath=Slot_path1+"\\"+f;
            rewrite_func_with_delta1(filepath,new_home_path);
        }

    }
    f1.remove();


}

void Calibrate_new_home::on_pushButton_clicked()
{
    //Update Path

    phone_profile_name1= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path1=main_directory1+"\\"+phone_profile_name1;
    Slot_path1=phone_profile_dir_path1+"\\"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QFile f8("C:\\Mach3\\homepath.tap");
    if(f8.exists())
        f8.remove();
    f8.open(QFile::WriteOnly | QFile::Text);
    QTextStream out8(&f8);
    out8<<Slot_path1;
    f8.close();
    QMessageBox::information(this,"Alert","Record Home Coordinates in Mach3");
    //ui->lineEdit->setText(Slot_path1);




}

void Calibrate_new_home::on_buttonBox_rejected()
{
    //Cancel button
    QMessageBox::information(this,"Alert","No Changes have been made");

}
