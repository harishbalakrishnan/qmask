#ifndef HKP_PROFILE_H
#define HKP_PROFILE_H

#include <QDialog>

namespace Ui {
class HKP_Profile;
}

class HKP_Profile : public QDialog
{
    Q_OBJECT
    
public:
    explicit HKP_Profile(QWidget *parent = 0);
    ~HKP_Profile();
    
private:
    Ui::HKP_Profile *ui;
};

#endif // HKP_PROFILE_H
