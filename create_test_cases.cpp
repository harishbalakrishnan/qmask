#include "create_test_cases.h"
#include "ui_create_test_cases.h"

//_____________________General Global Variables____________

QString main_directory5="G:\\Qt_Projects\\Main_Proj\\Prototype_new";

//Current Phone profile folder name
QString phone_profile_name5="Sony";


//Current phone profile directory path
QString phone_profile_dir_path5="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Sony";

//Slot path
QString Slot_path5="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Nokia1100\\Slot1";

//Test_Cases Path
QString test_case_folder_path="C:\\Mach3\\Test_Cases";

//________________________Slot Variables_________________________

QString Slot1Phone="A";
QString Slot2Phone="A";
QString Slot3Phone="A";
QString Slot4Phone="A";

//_________________________________________________________________



void append5(QString file1,QString file2)
{
    QFile f1(file1),f2(file2);
    if(!((f1.open(QFile::WriteOnly | QFile::Text | QFile::Append))))
    {
        return;
    }
    if(!((f2.open(QFile::ReadOnly | QFile::Text))))
    {
        return;
    }


    QTextStream in(&f2),out(&f1);
    QString wholetext=in.readAll();
    f2.close();
    out<<wholetext<<endl;
    out.flush();

    f1.close();

}

QString append_file_name(QString str2)
{
    int i,l,j=0,flag=1,m=0,y=0;
    i=2;
    l=str2.length();
    l=l-1;
    QString gg="";
    while(i>0)
    {
        if((str2[l]=='\\')||(str2[l]=='/'))
        {
            i--;
            l--;
            while((str2[l]!='\\'))
            {
                if(flag==1)
                {
                    gg[j]=str2[l];
                    flag=0;
                }
                l--;
            }
            j++;
            i--;
            l--;
            m=l;

            while((str2[l]!='\\'))
            {

                l--;
            }
            l++;

            for(y=l;y<=m;y++)
            {
                gg[j]=str2[y];
                j++;
            }
         }
        l--;
      }

      return gg;
}

Create_Test_Cases::Create_Test_Cases(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Create_Test_Cases)
{
    ui->setupUi(this);
    ui->comboBox->addItem("Slot1");
    ui->comboBox->addItem("Slot2");
    ui->comboBox->addItem("Slot3");
    ui->comboBox->addItem("Slot4");


    QDir present_dir(main_directory5);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    model=new QStringListModel;
    model->setStringList(directories);
    //ui->comboBox_2->setModel(model);
}

Create_Test_Cases::~Create_Test_Cases()
{
    delete ui;
}

void Create_Test_Cases::on_pushButton_3_clicked()
{
    //Show Available Functions
    available_functions= new QStringListModel(this);


    phone_profile_name5= ui->label_10->text();
    phone_profile_dir_path5=main_directory5+"\\"+phone_profile_name5;
    Slot_path5=phone_profile_dir_path5+"\\"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QDir presen_dir(Slot_path5);
    QStringList filters;
    filters << "*.tap";
    presen_dir.setNameFilters(filters);
    QStringList functions=presen_dir.entryList(QDir::Files);
    available_functions->setStringList(functions);
    ui->listView->setModel(available_functions);

}

void Create_Test_Cases::on_pushButton_clicked()
{
    //Primary ADD
    phone_profile_name5= ui->label_10->text();
    phone_profile_dir_path5=main_directory5+"\\"+phone_profile_name5;
    Slot_path5=phone_profile_dir_path5+"\\"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QDir presen_dir(Slot_path5);
    QStringList functions=presen_dir.entryList(QDir::Files);

    selected_functions = new QStringListModel(this);
    QModelIndex inde=ui->listView->currentIndex();
    QString temp= functions.value(inde.row());
    temp=Slot_path5+"\\"+temp;
    sel_functions.append(temp);
    selected_functions->setStringList(sel_functions);
    ui->listView_2->setModel(selected_functions);

}

void Create_Test_Cases::on_pushButton_4_clicked()
{
    //Primary CLEAR LIST
    QDir presen_dir(phone_profile_dir_path5);
    QStringList functions=presen_dir.entryList(QDir::Files);
    selected_functions = new QStringListModel(this);
    sel_functions.clear();
    selected_functions->setStringList(sel_functions);
    ui->listView_2->setModel(selected_functions);
}

void Create_Test_Cases::on_pushButton_2_clicked()
{
    //Create test case

    QString new_test_case="new_test_case";
    new_test_case=ui->lineEdit->text();
    QString Test_cases_path="C:\\Mach3\\Test_Cases";
    QString addition2="(";
    QString g="",n="";
    foreach(QString f,sel_functions)
    {

        g=append_file_name(f);
        n=addition2.append(g);
        n=n.append(",");
        addition2=n;

    }
    addition2.chop(1);
    addition2=addition2.append(")");

    QString filename1=Test_cases_path+"\\"+new_test_case + addition2 + ".m1s";
    QFile test_Script(filename1);
    test_Script.open(QFile::WriteOnly | QFile::Text);
    QTextStream out5(&test_Script);


    foreach(QString f,sel_functions)
    {
        out5<<"LoadFile(\""<<f<<"\")\n"<<"sleep(500)\n"<<"RunFile()\nsleep(500)\nWhile(IsMoving())\nWend\n";
    }
   test_Script.flush();
   test_Script.close();
}

void Create_Test_Cases::on_pushButton_5_clicked()
{
    //Show available test cases
    available_testCases = new QStringListModel(this);
    QDir presen_dir(test_case_folder_path);
    QStringList filters;
    filters << "*.m1s";
    presen_dir.setNameFilters(filters);
    QStringList testCases =presen_dir.entryList(QDir::Files);
    available_testCases->setStringList(testCases);
    ui->listView_3->setModel(available_testCases);

}

void Create_Test_Cases::on_pushButton_8_clicked()
{
    //Secondary Add
    QDir presen_dir(test_case_folder_path);
    QStringList testCases=presen_dir.entryList(QDir::Files);

    selected_testCases = new QStringListModel(this);
    QModelIndex inde=ui->listView_3->currentIndex();
    QString temp= testCases.value(inde.row());
    sel_testCases.append(temp);
    selected_testCases->setStringList(sel_testCases);
    ui->listView_4->setModel(selected_testCases);

}

void Create_Test_Cases::on_pushButton_9_clicked()
{
    //Delete
    QDir presen_dir(test_case_folder_path);
    QStringList testCases=presen_dir.entryList(QDir::Files);
    QModelIndex inde=ui->listView_3->currentIndex();
    QString temp= testCases.value(inde.row());
    QString test_case_file_path=test_case_folder_path+"\\"+temp;
    qDebug () << test_case_file_path;
    QFile f5(test_case_file_path);
    f5.remove();

}

void Create_Test_Cases::on_pushButton_6_clicked()
{
    //Seconday CLEAR LIST
    QDir presen_dir(test_case_folder_path);
    QStringList testCases=presen_dir.entryList(QDir::Files);
    selected_testCases = new QStringListModel(this);
    sel_testCases.clear();
    selected_testCases->setStringList(sel_testCases);
    ui->listView_4->setModel(selected_testCases);
}

void Create_Test_Cases::on_pushButton_7_clicked()
{
    //Create Delay File
    QString Delay_seconds=ui->lineEdit_3->text();
    QString Delay_file_name="Delay"+Delay_seconds+".m1s";
    QString file_name=test_case_folder_path+"\\"+Delay_file_name;
    QFile f1(file_name);
    f1.open(QFile::WriteOnly | QFile::Text);
    QTextStream out(&f1);
    out<<"Sleep("<<Delay_seconds<<") \n";
    f1.flush();
    f1.close();
}

void Create_Test_Cases::on_pushButton_10_clicked()
{
    //Create Derived Test Case
    QString new_test_case="new_test_case";
    new_test_case=ui->lineEdit_2->text();
    QString Test_cases_path="C:\\Mach3\\Test_Cases";
    QString filename1=Test_cases_path+"\\"+new_test_case+".m1s";
    foreach(QString f,sel_testCases)
    {
        QString filename2=Test_cases_path+"\\"+f;
        append5(filename1,filename2);

    }

}

void Create_Test_Cases::on_pushButton_11_clicked()
{
    //Slot1Fix
    QStringList phones;
    QDir present_dir(main_directory5);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    phones=directories;
    bool ok;
    QString phone = QInputDialog::getItem(this, tr("Slot1 Phone"),
                                         tr("Phone: "), phones, 0, false, &ok);
    Slot1Phone=phone;
    ui->label_11->setText(phone);
}

void Create_Test_Cases::on_pushButton_12_clicked()
{
    //Slot2Fix
    QStringList phones;
    QDir present_dir(main_directory5);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    phones=directories;
    bool ok;
    QString phone = QInputDialog::getItem(this, tr("Slot2 Phone"),
                                         tr("Phone: "), phones, 0, false, &ok);
    Slot2Phone=phone;
    ui->label_12->setText(phone);
}

void Create_Test_Cases::on_pushButton_13_clicked()
{
    //Slot3Fix
    QStringList phones;
    QDir present_dir(main_directory5);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    phones=directories;
    bool ok;
    QString phone = QInputDialog::getItem(this, tr("Slot3 Phone"),
                                         tr("Phone: "), phones, 0, false, &ok);
    Slot3Phone=phone;
    ui->label_13->setText(phone);

}

void Create_Test_Cases::on_pushButton_14_clicked()
{
    //Slot4Fix
    QStringList phones;
    QDir present_dir(main_directory5);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    phones=directories;
    QString phone = QInputDialog::getItem(this, tr("Slot4 Phone"),
                                         tr("Phone: "), phones, 0, false);
    Slot4Phone=phone;
    ui->label_14->setText(phone);
}

void Create_Test_Cases::on_comboBox_currentIndexChanged(int index)
{
    //Current Index Changed.
    switch(index)
    {
    case 0:ui->label_10->setText(Slot1Phone);break;
    case 1:ui->label_10->setText(Slot2Phone);break;
    case 2:ui->label_10->setText(Slot3Phone);break;
    case 3:ui->label_10->setText(Slot4Phone);break;
    }
}
