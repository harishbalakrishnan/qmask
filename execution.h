#ifndef EXECUTION_H
#define EXECUTION_H

#include <QDialog>
#include <QtCore>
#include <QtGui>

namespace Ui {
class Execution;
}

class Execution : public QDialog
{
    Q_OBJECT
    
public:
    explicit Execution(QWidget *parent = 0);
    ~Execution();
    
private slots:
    void on_pushButton_8_clicked();

private:
    Ui::Execution *ui;
    QStringListModel *available_testCases,*selected_testCases,*model1;
    QStringList functions,sel_functions,testCases,sel_testCases;
};

#endif // EXECUTION_H
