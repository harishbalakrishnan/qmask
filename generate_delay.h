#ifndef GENERATE_DELAY_H
#define GENERATE_DELAY_H

#include <QDialog>
#include <QDir>
#include <QtCore>
#include <QtGui>
#include <QFile>
#include <QTextStream>

namespace Ui {
class Generate_Delay;
}

class Generate_Delay : public QDialog
{
    Q_OBJECT
    
public:
    explicit Generate_Delay(QWidget *parent = 0);
    ~Generate_Delay();
    
private slots:
    void on_buttonBox_accepted();

private:
    Ui::Generate_Delay *ui;
    QStringListModel *available_functions,*selected_functions,*model;
    QStringList functions,sel_functions;
};

#endif // GENERATE_DELAY_H
