#ifndef FIXSLOTS_H
#define FIXSLOTS_H

#include <QDialog>

namespace Ui {
class FixSlots;
}

class FixSlots : public QDialog
{
    Q_OBJECT
    
public:
    explicit FixSlots(QWidget *parent = 0);
    ~FixSlots();
    
private:
    Ui::FixSlots *ui;
};

#endif // FIXSLOTS_H
