#include "hkp_profile.h"
#include "ui_hkp_profile.h"

HKP_Profile::HKP_Profile(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HKP_Profile)
{
    ui->setupUi(this);
    ui->comboBox->addItem("Profile1");
    ui->comboBox->addItem("Profile2");
    ui->comboBox->addItem("Profile3");
    ui->comboBox->addItem("Profile4");
}

HKP_Profile::~HKP_Profile()
{
    delete ui;
}
