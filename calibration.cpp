#include "calibration.h"
#include "ui_calibration.h"
#include <QMessageBox>


//Main Directory
QString main_director="G:\\Qt_Projects\\Main_Proj\\Prototype_new";

//Current Phone profile folder name
QString phone_profile_nam="Sony";


//Current phone profile directory path
QString phone_profile_dir_pat="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Sony";

QString display_path(QString path)
{
    int i=0;
    while(path[i]!=*path.end())
    {
        if(path[i]=='/')
            path[i]='\\';
        i++;
    }
    return path;
}



Calibration::Calibration(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Calibration)
{
    ui->setupUi(this);
    ui->pushButton->setToolTip("Creates a new folder with the profile name along with home.tap file <br> and creates four slot subfolders ");

}

Calibration::~Calibration()
{
    delete ui;
}

void Calibration::on_pushButton_clicked()
{
    // Create and calibrate home

    QString name_of_new_profile = ui->lineEdit->text();
    QDir present_dir(main_director);
    present_dir.mkdir(name_of_new_profile);
    QString newpath=main_director+"/"+name_of_new_profile;
    present_dir.setPath(newpath);
    present_dir.mkdir("Slot1");
    present_dir.mkdir("Slot2");
    present_dir.mkdir("Slot3");
    present_dir.mkdir("Slot4");
    newpath=display_path(newpath);
    newpath=newpath+"\\home.tap";
    QFile fil1(newpath);
    fil1.open(QFile::WriteOnly | QFile::Text);
    QTextStream out(&fil1);
    out<<"G0 X0 Y0 Z0 \n";
    fil1.close();


}

void Calibration::on_pushButton_3_clicked()
{
    //Generate Phone List... kinda refresh


}

void Calibration::on_pushButton_2_clicked()
{
    // Create New Basic Function


}
