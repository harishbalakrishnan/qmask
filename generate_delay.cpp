#include "generate_delay.h"
#include "ui_generate_delay.h"


//Main Directory
QString main_directory4="G:\\Qt_Projects\\Main_Proj\\Prototype_new";

//Current Phone profile folder name
QString phone_profile_name4="Sony";


//Current phone profile directory path
QString phone_profile_dir_path4="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Sony";

//Slot path
QString Slot_path4="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Nokia1100\\Slot1";

Generate_Delay::Generate_Delay(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Generate_Delay)
{
    ui->setupUi(this);
    QDir present_dir(main_directory4);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    model=new QStringListModel;
    model->setStringList(directories);
    ui->comboBox->setModel(model);
}

Generate_Delay::~Generate_Delay()
{
    delete ui;
}

void Generate_Delay::on_buttonBox_accepted()
{
    //Ok Button.
    phone_profile_name4= ui->comboBox->itemText(ui->comboBox->currentIndex());
    phone_profile_dir_path4=main_directory4+"/"+phone_profile_name4;
    QString Delay_seconds=ui->lineEdit->text();
    QString Delay_file_name="Delay"+Delay_seconds+".tap";
    QString file_name=phone_profile_dir_path4+"/"+Delay_file_name;
    QFile f1(file_name);
    f1.open(QFile::WriteOnly | QFile::Text);
    QTextStream out(&f1);
    out<<"G4 P"<<Delay_seconds<<" \n";
    f1.close();
    QString Slot1_path=phone_profile_dir_path4+"/Slot1/"+Delay_file_name;
    QString Slot2_path=phone_profile_dir_path4+"/Slot2/"+Delay_file_name;
    QString Slot3_path=phone_profile_dir_path4+"/Slot3/"+Delay_file_name;
    QString Slot4_path=phone_profile_dir_path4+"/Slot4/"+Delay_file_name;
    f1.copy(file_name,Slot1_path);
    f1.copy(file_name,Slot2_path);
    f1.copy(file_name,Slot3_path);
    f1.copy(file_name,Slot4_path);
}
