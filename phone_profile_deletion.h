#ifndef PHONE_PROFILE_DELETION_H
#define PHONE_PROFILE_DELETION_H

#include <QDialog>
#include <QDir>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>

namespace Ui {
class Phone_Profile_Deletion;
}

class Phone_Profile_Deletion : public QDialog
{
    Q_OBJECT
    
public:
    explicit Phone_Profile_Deletion(QWidget *parent = 0);
    ~Phone_Profile_Deletion();
    
private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::Phone_Profile_Deletion *ui;
    QStringListModel *available_functions,*selected_functions,*model;
    QStringList functions,sel_functions;
};

#endif // PHONE_PROFILE_DELETION_H
