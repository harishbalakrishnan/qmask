#include "phone_profile_deletion.h"
#include "ui_phone_profile_deletion.h"



QString main_directory7="G:\\Qt_Projects\\Main_Proj\\Prototype_new";

Phone_Profile_Deletion::Phone_Profile_Deletion(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Phone_Profile_Deletion)
{
    ui->setupUi(this);
    QDir present_dir(main_directory7);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    model=new QStringListModel;
    model->setStringList(directories);
    ui->comboBox->setModel(model);

}

Phone_Profile_Deletion::~Phone_Profile_Deletion()
{
    delete ui;
}

void Phone_Profile_Deletion::on_buttonBox_accepted()
{
    //Ok Button
    QString DirPath=main_directory7+"\\"+ui->comboBox->currentText();
    QDir p(DirPath);
    p.removeRecursively();
    QMessageBox::information(this,"Alert","Profile delete successful");

}

void Phone_Profile_Deletion::on_buttonBox_rejected()
{
    //Cancel Button
    QMessageBox::information(this,"Alert","No Profiles have been deleted");
}
