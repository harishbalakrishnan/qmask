#include "execution.h"
#include "ui_execution.h"
#include <QMessageBox>

//Test_Cases Path
QString test_case_folder_path2="C:\\Mach3\\Test_Cases";
QString temp_file_path="C:\\Mach3\\temp.m1s";

Execution::Execution(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Execution)
{
    ui->setupUi(this);
    available_testCases = new QStringListModel(this);
    QDir presen_dir(test_case_folder_path2);
    QStringList filters;
    filters << "*.m1s";
    presen_dir.setNameFilters(filters);
    QStringList testCases =presen_dir.entryList(QDir::Files);
    available_testCases->setStringList(testCases);
    ui->listView_3->setModel(available_testCases);
    ui->lineEdit->setText("1");
}

Execution::~Execution()
{
    delete ui;
}

void Execution::on_pushButton_8_clicked()
{
    //Create Test Case Executable file for MACH3
    QDir presen_dir(test_case_folder_path2);
    QStringList testCases=presen_dir.entryList(QDir::Files);
    QModelIndex inde=ui->listView_3->currentIndex();
    QString temp= testCases.value(inde.row());
    QString test_case_file_path=test_case_folder_path2+"\\"+temp;
    QFile f6(test_case_file_path);
    QFile f7(temp_file_path);
    if(f7.exists())
        f7.remove();
    int str_to_int=1,i;
    f7.open(QFile::WriteOnly | QFile::Text | QFile::Append);
    str_to_int=ui->lineEdit->text().toInt();
    f6.open(QFile::ReadOnly | QFile::Text);
    QTextStream in7(&f6),out7(&f7);
    QString data=in7.readAll();
    for(i=1;i<=str_to_int;i++)
        out7<<data;
    f6.close();
    f7.close();
    //f6.copy(test_case_file_path,temp_file_path);
    QMessageBox::information(this,"Success!","Created Executable file for MACH3.\nClick Execute button in MACH3 to execute the test case.");
}

