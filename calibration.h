#ifndef CALIBRATION_H
#define CALIBRATION_H

#include <QDialog>
#include <QDir>
#include <QtCore>
#include <QtGui>
#include <QFile>
#include <QTextStream>
#include <QClipboard>

namespace Ui {
class Calibration;
}

class Calibration : public QDialog
{
    Q_OBJECT
    
public:
    explicit Calibration(QWidget *parent = 0);
    ~Calibration();
    
private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Calibration *ui;
    QStringListModel *model;
};

#endif // CALIBRATION_H
