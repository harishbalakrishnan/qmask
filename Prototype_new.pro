#-------------------------------------------------
#
# Project created by QtCreator 2013-06-23T16:07:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Prototype_new
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    hkp_profile.cpp \
    phone_profile.cpp \
    calibration.cpp \
    test_case_creation.cpp \
    calibrate_new_home.cpp \
    calibrate_fresh_home.cpp \
    generate_delay.cpp \
    create_test_cases.cpp \
    execution.cpp \
    ttt.cpp \
    fixslots.cpp \
    phone_profile_deletion.cpp

HEADERS  += mainwindow.h \
    hkp_profile.h \
    phone_profile.h \
    Auxillary_Header.h \
    calibration.h \
    test_case_creation.h \
    calibrate_new_home.h \
    calibrate_fresh_home.h \
    generate_delay.h \
    create_test_cases.h \
    execution.h \
    ttt.h \
    fixslots.h \
    phone_profile_deletion.h

FORMS    += mainwindow.ui \
    hkp_profile.ui \
    phone_profile.ui \
    calibration.ui \
    test_case_creation.ui \
    calibrate_new_home.ui \
    calibrate_fresh_home.ui \
    generate_delay.ui \
    create_test_cases.ui \
    execution.ui \
    ttt.ui \
    fixslots.ui \
    phone_profile_deletion.ui

RESOURCES += \
    Res.qrc
