#ifndef CALIBRATE_FRESH_HOME_H
#define CALIBRATE_FRESH_HOME_H

#include <QDialog>
#include <QFile>
#include <QTextStream>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include <QDir>

namespace Ui {
class Calibrate_fresh_home;
}

class Calibrate_fresh_home : public QDialog
{
    Q_OBJECT
    
public:
    explicit Calibrate_fresh_home(QWidget *parent = 0);
    ~Calibrate_fresh_home();
    
private slots:
    void on_buttonBox_accepted();

    void on_pushButton_clicked();

private:
    Ui::Calibrate_fresh_home *ui;
    QStringListModel *available_functions,*selected_functions,*model;
    QStringList functions,sel_functions;
};

#endif // CALIBRATE_FRESH_HOME_H
