#ifndef CALIBRATE_NEW_HOME_H
#define CALIBRATE_NEW_HOME_H

#include <QDialog>
#include <QFile>
#include <QTextStream>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>
#include <QDir>

namespace Ui {
class Calibrate_new_home;
}

class Calibrate_new_home : public QDialog
{
    Q_OBJECT
    
public:
    explicit Calibrate_new_home(QWidget *parent = 0);
    ~Calibrate_new_home();
    
private slots:
    void on_buttonBox_accepted();

    void on_pushButton_clicked();

    void on_buttonBox_rejected();

private:
    Ui::Calibrate_new_home *ui;
    QStringListModel *available_functions,*selected_functions,*model;
    QStringList functions,sel_functions;
};

#endif // CALIBRATE_NEW_HOME_H
