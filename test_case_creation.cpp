#include "test_case_creation.h"
#include "ui_test_case_creation.h"
#include <QDebug>
#include <QMessageBox>
#include "calibrate_new_home.h"
#include "calibrate_fresh_home.h"
#include "generate_delay.h"

//Main Directory
QString main_directory="G:\\Qt_Projects\\Main_Proj\\Prototype_new";

//Current Phone profile folder name
QString phone_profile_name="Sony";


//Current phone profile directory path
QString phone_profile_dir_path="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Sony";

//Slot path
QString Slot_path="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Nokia1100\\Slot1";

float find_delta(QString path,QChar c)
{
    //This function finds delta.

    //Variables
    int i=0;
    float str_to_float=1;
    QChar uc,lc;
    QString temp_string="";

    //Code
    lc=c.toLower();
    uc=c.toUpper();
    QFile file1(path);
    file1.open(QFile::ReadWrite | QFile::Text);

    QTextStream inout(&file1);
    QString text=inout.readAll();
    while ( text[i]!=*text.end() )
    {
        if(text[i]==lc||text[i]==uc)
            {
                i++;
                while(text[i]!=' ')
                {
                    temp_string=temp_string.append(text[i]);
                    i++;
                }
                str_to_float=temp_string.toFloat();
                str_to_float=(str_to_float*1000);
                str_to_float=(int)str_to_float;
                str_to_float=str_to_float/1000;
                temp_string="";
                break;
            }
            i++;
   }
    file1.close();
    return (str_to_float);

}

void rewrite_func_with_delta(QString path,QString path1)
{
    //Variables required
    QString temp_string="";
    int i=0,pos=0,len=0,indicator=0;
    float str_to_float=0,x_diff=0,y_diff=0,z_diff=0;

    //Code
    x_diff=find_delta(path1,'X');
    y_diff=find_delta(path1,'Y');
    z_diff=find_delta(path1,'Z');

    QFile file1(path);
     if(!file1.open(QFile::ReadWrite | QFile::Text))
         qDebug () << "Could not open file";

     QTextStream inout(&file1);
     QString text=inout.readAll();

     while ( text[i]!=*text.end() )
     {
         if(text[i]=='X'||text[i]=='Y'||text[i]=='Z'||text[i]=='x'||text[i]=='y'||text[i]=='z')
             {
                 indicator=i;
                 i++;
                 pos =i;
                 while(text[i]!=' ')
                 {
                     temp_string=temp_string.append(text[i]);
                     i++;
                 }
                 len=temp_string.length();
                 str_to_float=temp_string.toFloat();
                 if(text[indicator]=='X'||text[indicator]=='x')
                    str_to_float+=x_diff;
                 else if(text[indicator]=='Y'||text[indicator]=='y')
                     str_to_float+=y_diff;
                 else
                     str_to_float+=z_diff;
                 str_to_float=(str_to_float*1000);
                 str_to_float=(int)str_to_float;
                 str_to_float=str_to_float/1000;
                 QString tt;
                 if((str_to_float<0.001)&&(str_to_float>-0.001))
                     tt="0";
                 else
                     QTextStream(&tt) <<str_to_float;

                 temp_string="";
                 text=text.replace(pos,len,tt);
                 i=pos+tt.length();

             }

             i++;
    }
    file1.remove();
    file1.close();
    file1.open(QFile::WriteOnly | QFile::Text |QFile::Append);
    QTextStream out(&file1);
    out<<text;
    out.flush();
    file1.close();

}


void append(QString file1,QString file2)
{
    QFile f1(file1),f2(file2);
    if(!((f1.open(QFile::WriteOnly | QFile::Text | QFile::Append))))
    {
        qDebug () << "Could not open file1";
        return;
    }
    if(!((f2.open(QFile::ReadOnly | QFile::Text))))
    {
        qDebug () << "Could not open file2";
        return;
    }


    QTextStream in(&f2),out(&f1);
    QString wholetext=in.readAll();
    f2.close();
    out<<wholetext<<endl;
    out.flush();

    f1.close();

}


Test_Case_Creation::Test_Case_Creation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Test_Case_Creation)
{
    ui->setupUi(this);

    ui->pushButton->setToolTip("Adds the current selected function to Selected-functions-List");
    ui->pushButton_2->setToolTip("Appends all the functions in the Selected functions list and creates a function with the given name");
    ui->pushButton_3->setToolTip("Shows the functions in the slot folder of the phone profile");
    ui->pushButton_4->setToolTip("Clears the Selected functions List so that a new combination of functions can be derived");
    ui->pushButton_5->setToolTip("Adds the recorded function to the slots if home is calibrated");
    ui->pushButton_6->setToolTip("Calibrates/Recalibrates home ");



    ui->comboBox->addItem("Slot1");
    ui->comboBox->addItem("Slot2");
    ui->comboBox->addItem("Slot3");
    ui->comboBox->addItem("Slot4");


    QDir present_dir(main_directory);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    model=new QStringListModel;
    model->setStringList(directories);
    ui->comboBox_2->setModel(model);
    QString filll=main_directory+"/temp.tap";
    QFile ff2(filll);
    if(ff2.exists())
        ff2.remove();
    ff2.open(QFile::ReadWrite);
    ff2.close();
}

Test_Case_Creation::~Test_Case_Creation()
{

    delete ui;
}

void Test_Case_Creation::on_pushButton_3_clicked()
{
    //Show Available Functions.

    available_functions= new QStringListModel(this);


    phone_profile_name= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path=main_directory+"/"+phone_profile_name;
    Slot_path=phone_profile_dir_path+"/"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QDir presen_dir(Slot_path);
    QStringList filters;
    filters << "*.tap";
    presen_dir.setNameFilters(filters);
    QStringList functions=presen_dir.entryList(QDir::Files);
    available_functions->setStringList(functions);
    ui->listView->setModel(available_functions);
}

void Test_Case_Creation::on_comboBox_2_currentIndexChanged(const QString &arg1)
{

}

void Test_Case_Creation::on_pushButton_clicked()
{
    //Add PB
    phone_profile_name= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path=main_directory+"/"+phone_profile_name;
    Slot_path=phone_profile_dir_path+"/"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QDir presen_dir(Slot_path);
    QStringList functions=presen_dir.entryList(QDir::Files);

    selected_functions = new QStringListModel(this);
    QModelIndex inde=ui->listView->currentIndex();
    QString temp= functions.value(inde.row());
    sel_functions.append(temp);
    selected_functions->setStringList(sel_functions);
    ui->listView_2->setModel(selected_functions);

}

void Test_Case_Creation::on_pushButton_2_clicked()
{

    // Create Derived function button
    Slot_path=phone_profile_dir_path+"/"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QString new_func_name="new_func";
    new_func_name=ui->lineEdit->text();
    QString Slot_home=Slot_path+"/home.tap";
    QString filename1=Slot_path+"/"+new_func_name+".tap";   //filename1 has the new function's path
    foreach(QString f,sel_functions)                        //Slot_path has the current slot's path
    {
        QString filename2=Slot_path+"/"+f;
        append(filename1,filename2);
        qDebug () <<"Here I am";

    }


    //Copying it into other slots
    float x=0,x1=0,y=0,y1=0,z=0,z1=0;
    QString homepath=phone_profile_dir_path+"/home.tap";
    QFile f1(filename1),f2(homepath);
    f1.open(QFile::ReadOnly | QFile::Text);
    f2.remove();
    f2.open(QFile::WriteOnly | QFile::Text);
    QTextStream out(&f2);
    qDebug () <<"Here";
    QString path=phone_profile_dir_path+"/"+"Slot1/home.tap";
    QString slot_temp_path=phone_profile_dir_path+"/Slot1";
    QFile homtest(path);
    if((homtest.exists())&&(slot_temp_path!=Slot_path))
    {
        x=find_delta(Slot_home,'X');
        x1=find_delta(path,'X');
        x=x1-x;
        y=find_delta(Slot_home,'Y');
        y1=find_delta(path,'Y');
        y=y1-y;
        z=find_delta(Slot_home,'Z');
        z1=find_delta(path,'Z');
        z=z1-z;
        out<<"G0 X"<<x<<" Y"<<y<<" Z"<<z<<" \n";

    path=phone_profile_dir_path+"/Slot1/"+new_func_name+".tap";
    f1.close();
    f1.copy(filename1,path);
    f2.close();
    rewrite_func_with_delta(path,homepath);

    f2.remove();
    f2.open(QFile::WriteOnly | QFile::Text);
    }
    path=phone_profile_dir_path+"/Slot2/home.tap";
    homtest.close();
    slot_temp_path=phone_profile_dir_path+"/Slot2";
    QFile homtest1(path);
    if((homtest1.exists())&&(slot_temp_path!=Slot_path))
    {
        x=find_delta(Slot_home,'X');
        x1=find_delta(path,'X');
        x=x1-x;
        y=find_delta(Slot_home,'Y');
        y1=find_delta(path,'Y');
        y=y1-y;
        z=find_delta(Slot_home,'Z');
        z1=find_delta(path,'Z');
        z=z1-z;
        out<<"G0 X"<<x<<" Y"<<y<<" Z"<<z<<" \n";

    path=phone_profile_dir_path+"/"+"Slot2/"+new_func_name+".tap";
    f1.close();
    f1.copy(filename1,path);
    f2.close();
    rewrite_func_with_delta(path,homepath);
    f2.remove();
    f2.open(QFile::WriteOnly | QFile::Text);
    }
    homtest1.close();
    path=phone_profile_dir_path+"/Slot3/home.tap";
    slot_temp_path=phone_profile_dir_path+"/Slot3";
    QFile homtest2(path);
    if((homtest2.exists())&&(slot_temp_path!=Slot_path))
    {
        x=find_delta(Slot_home,'X');
        x1=find_delta(path,'X');
        x=x1-x;
        y=find_delta(Slot_home,'Y');
        y1=find_delta(path,'Y');
        y=y1-y;
        z=find_delta(Slot_home,'Z');
        z1=find_delta(path,'Z');
        z=z1-z;
        out<<"G0 X"<<x<<" Y"<<y<<" Z"<<z<<" \n";

    path=phone_profile_dir_path+"/"+"Slot3/"+new_func_name+".tap";
    f1.close();
    f1.copy(filename1,path);
    f2.close();
    rewrite_func_with_delta(path,homepath);
    f2.remove();
    f2.open(QFile::WriteOnly | QFile::Text);
    }
    homtest2.close();
    path=phone_profile_dir_path+"/"+"Slot4/home.tap";
    slot_temp_path=phone_profile_dir_path+"/Slot4";
    QFile homtest3(path);
    if((homtest3.exists())&&(slot_temp_path!=Slot_path))
    {
        x=find_delta(Slot_home,'X');
        x1=find_delta(path,'X');
        x=x1-x;
        y=find_delta(Slot_home,'Y');
        y1=find_delta(path,'Y');
        y=y1-y;
        z=find_delta(Slot_home,'Z');
        z1=find_delta(path,'Z');
        z=z1-z;
        out<<"G0 X"<<x<<" Y"<<y<<" Z"<<z<<" \n";
        path=phone_profile_dir_path+"/Slot4/"+new_func_name+".tap";
        f1.close();
        f1.copy(filename1,path);
        f2.close();
        rewrite_func_with_delta(path,homepath);
        f2.remove();
        f2.open(QFile::WriteOnly | QFile::Text);
    }
    homtest3.close();
    f1.close();
    float x_d=0,y_d=0,z_d=0;
    x_d=-find_delta(Slot_home,'x');
    y_d=-find_delta(Slot_home,'y');
    z_d=-find_delta(Slot_home,'z');
    out<<"G0 X"<<x_d<<" Y"<<y_d<<" Z"<<z_d<<" \n";
    f2.close();
    QString tem=phone_profile_dir_path+"/"+new_func_name+".tap";
    f1.copy(filename1,tem);
    rewrite_func_with_delta(tem,homepath);
    //f2.flush();
    f2.remove();
    f2.close();
    f2.open(QFile::WriteOnly | QFile::Text);
    out<<"G0 X0 Y0 Z0 \n";
    f2.close();
    f1.close();



}

void Test_Case_Creation::on_pushButton_4_clicked()
{
    // Clear list slot
    QDir presen_dir(phone_profile_dir_path);
    QStringList functions=presen_dir.entryList(QDir::Files);

    selected_functions = new QStringListModel(this);
    sel_functions.clear();
    selected_functions->setStringList(sel_functions);
    ui->listView_2->setModel(selected_functions);
}

void Test_Case_Creation::on_pushButton_5_clicked()
{
    //Add recorded Function to profile primary location

    QString primary_home_location=phone_profile_dir_path+"\\home.tap";

    phone_profile_name= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path=main_directory+"/"+phone_profile_name;
    Slot_path=phone_profile_dir_path+"\\"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QDir presen_dir(phone_profile_dir_path);
    QDir m_dir(main_directory);
    QString fil=main_directory+"\\temp.tap";
    QString func_name=ui->lineEdit_2->text();
    QString fil2=Slot_path+"\\"+func_name+".tap";
    QFile f1(fil2),f2(fil);
    f1.open(QFile::WriteOnly | QFile::Text);
    f2.open(QFile::ReadOnly | QFile::Text);
    QTextStream in(&f2),out(&f1);
    QString wholetext=in.readAll();
    out<<wholetext<<endl;
    out.flush();
    f2.close();
    f1.close();

    QString home_function_path=phone_profile_dir_path+"\\"+func_name+".tap";
    QString home_path1=Slot_path+"\\home.tap";
    float x=0,y=0,z=0;
    x=-find_delta(home_path1,'X');
    y=-find_delta(home_path1,'Y');
    z=-find_delta(home_path1,'Z');
    f1.copy(fil2,home_function_path);
    QFile f3(primary_home_location);
    f3.remove();
    f3.open(QFile::WriteOnly | QFile::Text);
    QTextStream out1(&f3);
    out1<<"G0 X"<<x<<" Y"<<y<<" Z"<<z<<" \n";
    f3.close();
    rewrite_func_with_delta(home_function_path,primary_home_location);
    f3.remove();
    f3.open(QFile::WriteOnly | QFile::Text);
    out1<<"G0 X0 Y0 Z0 \n";
    f3.close();


    //Add This file to all the four slots and Offset it.

    //1.Copying it
    QString path=phone_profile_dir_path+"/"+"Slot1/home.tap";
    QString path1;
    QString temp_slot_path;
   // QFile f4(primary_home_location);
    //QTextStream out2(&f4);
    //f4.open(QFile::WriteOnly | QFile::Text);
    temp_slot_path=phone_profile_dir_path+"\\Slot1";
    QFile homtest(path);
    if((homtest.exists())&&(temp_slot_path!=Slot_path))
    {
    path=temp_slot_path+"\\"+func_name+".tap";
    path1=phone_profile_dir_path+"/Slot1/home.tap";
    f1.copy(home_function_path,path);
    rewrite_func_with_delta(path,path1);
    }
    path=phone_profile_dir_path+"/Slot2/home.tap";
    homtest.close();
    QFile homtest1(path);
    temp_slot_path=phone_profile_dir_path+"\\Slot2";

    if((homtest1.exists())&&(temp_slot_path!=Slot_path))
    {
    path=temp_slot_path+"/"+func_name+".tap";
    path1=phone_profile_dir_path+"/Slot2/home.tap";
    f1.copy(home_function_path,path);
    rewrite_func_with_delta(path,path1);
    }
    homtest1.close();
    path=phone_profile_dir_path+"/Slot3/home.tap";
    QFile homtest2(path);
    temp_slot_path=phone_profile_dir_path+"\\Slot3";
    if((homtest2.exists())&&(temp_slot_path!=Slot_path))
    {
    path1=temp_slot_path+"/"+func_name+".tap";
    f1.copy(home_function_path,path1);
    rewrite_func_with_delta(path1,path);
    }
    homtest2.close();
    path=phone_profile_dir_path+"/"+"Slot4/home.tap";
    temp_slot_path=phone_profile_dir_path+"\\Slot4";
    QFile homtest3(path);
    if((homtest3.exists())&&(temp_slot_path!=Slot_path))
    {
        path1=temp_slot_path+"\\"+func_name+".tap";
        f1.copy(home_function_path,path1);
        rewrite_func_with_delta(path1,path);
    }
    homtest3.close();
    f1.close();


    //Adding the function to the primary location and ofsetting it.




}

void Test_Case_Creation::on_pushButton_6_clicked()
{
    //Calibrate Home according to slot.

    phone_profile_name= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path=main_directory+"\\"+phone_profile_name;
    Slot_path=phone_profile_dir_path+"\\"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QString current_home_path=Slot_path+"\\home.tap";
    QString new_home_path=Slot_path+"\\home2.tap";
    QFile f1(current_home_path);
    if(f1.exists())
    {
        //QFile f2(new_home_path);
        //f2.open(QFile::WriteOnly | QFile::Text);
        //f2.close();
        f1.copy(current_home_path,new_home_path);
        f1.close();
        f1.remove();
        f1.open(QFile::WriteOnly | QFile::Text);
        f1.close();

        QMessageBox::information(this,"Alert","Home already calibrated");
        QString new_home_path=Slot_path+"\\home2.tap";
        Calibrate_new_home new_dialog;
        new_dialog.setModal(true);
        new_dialog.exec();
        //QMessageBox::information(this,"Copy","Copy the path from the line edit");




    }
    else
    {
        f1.open(QFile::WriteOnly | QFile::Text);
        f1.close();
        Calibrate_fresh_home new_dialog1;
        new_dialog1.setModal(true);
        new_dialog1.exec();
        QString new_home_path=Slot_path+"\\home.tap";

    }



}

void Test_Case_Creation::on_comboBox_2_currentIndexChanged(int index)
{
    QString phone_profile_name= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path=main_directory+"/"+phone_profile_name;
    QString slot1_path=phone_profile_dir_path+"/Slot1";
    QString slot2_path=phone_profile_dir_path+"/Slot2";
    QString slot3_path=phone_profile_dir_path+"/Slot3";
    QString slot4_path=phone_profile_dir_path+"/Slot4";
    QString slot1_home=slot1_path+"/home.tap";
    QString slot2_home=slot2_path+"/home.tap";
    QString slot3_home=slot3_path+"/home.tap";
    QString slot4_home=slot4_path+"/home.tap";
    QFile file1(slot1_home),file2(slot2_home),file3(slot3_home),file4(slot4_home);
    if( file1.exists())
        ui->label_8->setText("Home for Slot 1 calibrated");
    else
        ui->label_8->setText("Home for Slot 1 not calibrated");
    if( file2.exists())
        ui->label_9->setText("Home for Slot 2 calibrated");
    else
        ui->label_9->setText("Home for Slot 2 not calibrated");
    if( file3.exists())
        ui->label_10->setText("Home for Slot 3 calibrated");
    else
        ui->label_10->setText("Home for Slot 3 not calibrated");
    if( file4.exists())
        ui->label_11->setText("Home for Slot 4 calibrated");
    else
        ui->label_11->setText("Home for Slot 4 not calibrated");
}

void Test_Case_Creation::on_pushButton_7_clicked()
{
    // Generate Delay.
    Generate_Delay delay_dialog;
    delay_dialog.setModal(true);
    delay_dialog.exec();

}

void Test_Case_Creation::on_pushButton_8_clicked()
{
    //Deleting functions.
    phone_profile_name= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path=main_directory+"\\"+phone_profile_name;
    Slot_path=phone_profile_dir_path+"\\"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QDir presen_dir(Slot_path);
    QDir parent_dir(phone_profile_dir_path);
    QStringList functions=presen_dir.entryList(QDir::Files);

    selected_functions = new QStringListModel(this);
    QModelIndex inde=ui->listView->currentIndex();
    QString temp= functions.value(inde.row());
    qDebug () <<temp;
    QString delpath1=phone_profile_dir_path+"\\"+temp;
    QString delpath2=phone_profile_dir_path+"\\Slot1\\"+temp;
    QString delpath3=phone_profile_dir_path+"\\Slot2\\"+temp;
    QString delpath4=phone_profile_dir_path+"\\Slot3\\"+temp;
    QString delpath5=phone_profile_dir_path+"\\Slot4\\"+temp;
    if(temp=="home.tap")
    {
        qDebug() << presen_dir.removeRecursively();

        qDebug() << parent_dir.mkdir(ui->comboBox->itemText(ui->comboBox->currentIndex()));
        qDebug() << "I am Here";
    }
    else
    {
        qDebug () <<delpath4;

        QFile ff1(delpath1),ff2(delpath2),ff3(delpath3),ff4(delpath4),ff5(delpath5);
        if(ff1.exists())
            ff1.remove();
        if(ff2.exists())
            ff2.remove();
        if(ff3.exists())
            ff3.remove();
        if(ff4.exists())
            ff4.remove();
        if(ff5.exists())
            ff5.remove();
    }




}
