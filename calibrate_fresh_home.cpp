#include "calibrate_fresh_home.h"
#include "ui_calibrate_fresh_home.h"
#include <QMessageBox>
#include <QDir>

//Main Directory
QString main_directory2="G:\\Qt_Projects\\Main_Proj\\Prototype_new";

//Current Phone profile folder name
QString phone_profile_name2="Sony";


//Current phone profile directory path
QString phone_profile_dir_path2="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Sony";

//Slot path
QString Slot_path2="G:\\Qt_Projects\\Main_Proj\\Prototype_new\\Nokia1100\\Slot1";

float find_delta2(QString path,QChar c)
{
    //This function finds delta.

    //Variables
    int i=0;
    float str_to_float=1;
    QChar uc,lc;
    QString temp_string="";

    //Code
    lc=c.toLower();
    uc=c.toUpper();
    QFile file1(path);
    file1.open(QFile::ReadWrite | QFile::Text);

    QTextStream inout(&file1);
    QString text=inout.readAll();
    while ( text[i]!=*text.end() )
    {
        if(text[i]==lc||text[i]==uc)
            {
                i++;
                while(text[i]!=' ')
                {
                    temp_string=temp_string.append(text[i]);
                    i++;
                }
                str_to_float=temp_string.toFloat();
                str_to_float=(str_to_float*1000);
                str_to_float=(int)str_to_float;
                str_to_float=str_to_float/1000;
                temp_string="";
                break;
            }
            i++;
   }
    file1.close();
    return (str_to_float);

}

void rewrite_func_with_delta2(QString path,QString path1)
{
    //Variables required
    QString temp_string="";
    int i=0,pos=0,len=0,indicator=0;
    float str_to_float=0,x_diff=0,y_diff=0,z_diff=0;

    //Code
    x_diff=find_delta2(path1,'X');
    y_diff=find_delta2(path1,'Y');
    z_diff=find_delta2(path1,'Z');

    QFile file1(path);
     if(!file1.open(QFile::ReadWrite | QFile::Text))
         qDebug () << "Could not open file";

     QTextStream inout(&file1);
     QString text=inout.readAll();

     while ( text[i]!=*text.end() )
     {
         if(text[i]=='X'||text[i]=='Y'||text[i]=='Z'||text[i]=='x'||text[i]=='y'||text[i]=='z')
             {
                 indicator=i;
                 i++;
                 pos =i;
                 while(text[i]!=' ')
                 {
                     temp_string=temp_string.append(text[i]);
                     i++;
                 }
                 len=temp_string.length();
                 str_to_float=temp_string.toFloat();
                 if(text[indicator]=='X'||text[indicator]=='x')
                    str_to_float+=x_diff;
                 else if(text[indicator]=='Y'||text[indicator]=='y')
                     str_to_float+=y_diff;
                 else
                     str_to_float+=z_diff;
                 str_to_float=(str_to_float*1000);
                 str_to_float=(int)str_to_float;
                 str_to_float=str_to_float/1000;
                 QString tt;
                 if((str_to_float<0.001)&&(str_to_float>-0.001))
                     tt="0";
                 else
                     QTextStream(&tt) <<str_to_float;

                 temp_string="";
                 text=text.replace(pos,len,tt);
                 i=pos+tt.length();

             }

             i++;
    }
    file1.remove();
    file1.close();
    file1.open(QFile::WriteOnly | QFile::Text |QFile::Append);
    QTextStream out(&file1);
    out<<text;
    out.flush();
    file1.close();

}


Calibrate_fresh_home::Calibrate_fresh_home(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Calibrate_fresh_home)
{
    ui->setupUi(this);
    ui->comboBox->addItem("Slot1");
    ui->comboBox->addItem("Slot2");
    ui->comboBox->addItem("Slot3");
    ui->comboBox->addItem("Slot4");

    QDir present_dir(main_directory2);
    QStringList directories=present_dir.entryList(QDir::Dirs);
    directories.removeFirst();
    directories.removeFirst();
    model=new QStringListModel;
    model->setStringList(directories);
    ui->comboBox_2->setModel(model);

}

Calibrate_fresh_home::~Calibrate_fresh_home()
{
    delete ui;
}

void Calibrate_fresh_home::on_buttonBox_accepted()
{
    // Ok button

    phone_profile_name2= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path2=main_directory2+"\\"+phone_profile_name2;
    Slot_path2=phone_profile_dir_path2+"\\"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QString current_home_path=Slot_path2+"\\home.tap";

    QDir pres_dir(phone_profile_dir_path2);
    QStringList funct=pres_dir.entryList(QDir::Files);

    foreach(QString f,funct)                        //Slot_path has the current slot's path
    {
        if(f!="home.tap")
        {
            QString filepath=phone_profile_dir_path2+"\\"+f;
            QString filepath2=Slot_path2+"\\"+f;
            QFile f11(filepath);
            f11.copy(filepath,filepath2);
            f11.close();
            rewrite_func_with_delta2(filepath2,current_home_path);
        }

    }


}

void Calibrate_fresh_home::on_pushButton_clicked()
{
    //Generate path button.
    phone_profile_name2= ui->comboBox_2->itemText(ui->comboBox_2->currentIndex());
    phone_profile_dir_path2=main_directory2+"\\"+phone_profile_name2;
    Slot_path2=phone_profile_dir_path2+"\\"+ui->comboBox->itemText(ui->comboBox->currentIndex());
    QFile f8("C:\\Mach3\\homepath.tap");
    if(f8.exists())
        f8.remove();
    f8.open(QFile::WriteOnly | QFile::Text);
    QTextStream out8(&f8);
    out8<<Slot_path2;
    f8.close();
    QMessageBox::information(this,"Alert","Record Home Coordinates in Mach3");

}
