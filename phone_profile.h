#ifndef PHONE_PROFILE_H
#define PHONE_PROFILE_H

#include <QDialog>

namespace Ui {
class Phone_Profile;
}

class Phone_Profile : public QDialog
{
    Q_OBJECT
    
public:
    explicit Phone_Profile(QWidget *parent = 0);
    ~Phone_Profile();
    
private slots:
    void on_buttonBox_accepted();

private:
    Ui::Phone_Profile *ui;
};

#endif // PHONE_PROFILE_H
