#ifndef TEST_CASE_CREATION_H
#define TEST_CASE_CREATION_H

#include <QDialog>
#include <QtCore>
#include <QtGui>

namespace Ui {
class Test_Case_Creation;
}

class Test_Case_Creation : public QDialog
{
    Q_OBJECT
    
public:
    explicit Test_Case_Creation(QWidget *parent = 0);
    ~Test_Case_Creation();
    
private slots:
    void on_pushButton_3_clicked();

    void on_comboBox_2_currentIndexChanged(const QString &arg1);

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_comboBox_2_currentIndexChanged(int index);



    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

private:
    Ui::Test_Case_Creation *ui;
    QStringListModel *available_functions,*selected_functions,*model;
    QStringList functions,sel_functions;
};

#endif // TEST_CASE_CREATION_H
