#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();
    
private slots:
    void on_actionHKP_profile_triggered();

    void on_actionPhone_profile_triggered();

    void on_actionTest_case_creation_triggered();

    void on_actionCalibration_triggered();

    void on_actionCreate_Test_Cases_triggered();

    void on_actionTest_Case_Execution_triggered();

    void on_actionPhone_Profile_Deletion_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
