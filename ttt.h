#ifndef TTT_H
#define TTT_H

#include <QDialog>

namespace Ui {
class TTT;
}

class TTT : public QDialog
{
    Q_OBJECT
    
public:
    explicit TTT(QWidget *parent = 0);
    ~TTT();
    
private:
    Ui::TTT *ui;
};

#endif // TTT_H
